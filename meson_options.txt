option('rng_backend', type : 'combo', choices : ['internal_mersenne', 'stl_mt19937', 'intel_mkl'], description : 'The backend used for the random number generator provided by loadleveller')
